//
//  GameScene.swift
//  MapSK
//
//  Created by Juan Carlos Cubillos Ocampo on 8/4/15.
//  Copyright (c) 2015 Juan Carlos Cubillos Ocampo. All rights reserved.
//

import SpriteKit

class GameScene: SKScene {
    
    var rect = CGRectMake(50, 50, 100, 100);
    
    override func didMoveToView(view: SKView) {
        self.size = CGSize(width: view.frame.width, height: view.frame.height)
        /* Set the scale mode to scale to fit the window */
        self.scaleMode = SKSceneScaleMode.ResizeFill
        
        //rect = CGRectMake(50, 50, self.size.width-100, self.size.height-100);
        
        rect = CGRectMake(0, 0, self.size.width-100, self.size.height-100);
        
        //self.anchorPoint = CGPoint(x:0.5, y:0.5)
        redrawTriangles()
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        redrawTriangles()
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func redrawTriangles() {
        
        //let rect = CGRectMake(50, 50, self.frame.size.width-100, self.frame.size.height-100);
        //let rect = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
        //let rect = CGRectMake(0, 0, 300, 300);
        
        
        var points = randomPointsWithLength(10)
        //var points = randomPointsLloyd(20)
        
        let delaunayVoronoi = DelaunayVoronoi( points: points as [AnyObject], plotBounds:rect)
        removeAllChildren()
        
        // draw border
        var path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, 0, 0);
        CGPathAddLineToPoint(path, nil, self.size.width, 0)
        CGPathAddLineToPoint(path, nil, self.frame.size.width, self.frame.size.height)
        CGPathAddLineToPoint(path, nil, 0, self.frame.size.height)
        CGPathAddLineToPoint(path, nil, 0, 0)
        var line = SKShapeNode()
        line.path = path
        line.lineWidth = 0.5
        line.strokeColor = UIColor.yellowColor()
        addChild(line)
        
        // draw map border
        path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, rect.minX, rect.minY);
        CGPathAddLineToPoint(path, nil, rect.maxX, rect.minY)
        CGPathAddLineToPoint(path, nil, rect.maxX, rect.maxY)
        CGPathAddLineToPoint(path, nil, rect.minX, rect.maxY)
        CGPathAddLineToPoint(path, nil, rect.minX, rect.minY)
        line = SKShapeNode()
        line.path = path
        line.lineWidth = 0.5
        line.strokeColor = UIColor.greenColor()
        addChild(line)
        
        for value in points {
            let point = value.CGPointValue()
            var point2 = point;
            point2.x = point2.x+5
            let line = SKShapeNode()
            line.path = linePathWithStartPoint(point, andEndPoint:point2)
            line.lineWidth = 0.5;
            line.strokeColor = UIColor.redColor()
            addChild(line)
        }
        
        for e in delaunayVoronoi.edges {
        let edge = e as! DelaunayEdge
        let line = SKShapeNode()
        line.path = linePathWithStartPoint(edge.delaunayLine().p0, andEndPoint:edge.delaunayLine().p1)
        line.lineWidth = 0.5;
        line.strokeColor = UIColor.whiteColor()
        addChild(line)
        }
        
        
        for region in delaunayVoronoi.regions() {
            let nsArray = region as! NSArray
            
            if nsArray.count > 0 {
                var path = CGPathCreateMutable();
                var p0 = (nsArray[0] as! NSValue).CGPointValue()
                CGPathMoveToPoint(path, nil, p0.x, p0.y);
                
                for i in 1...nsArray.count-1 {
                    let point = (nsArray[i] as! NSValue).CGPointValue()
                    CGPathAddLineToPoint(path, nil, point.x, point.y);
                }
                //CGPathAddLineToPoint(path, nil, p0.x, p0.y);
                
                let line = SKShapeNode()
                line.path = path
                line.lineWidth = 0.5;
                line.strokeColor = UIColor.blueColor()
                addChild(line)
            }
        }
        
        /*for e in delaunayVoronoi.edges {
        let edge = e as! DelaunayEdge
        if edge.voronoiEdge() != nil {
        let line = SKShapeNode()
        line.path = linePathWithStartPoint(edge.voronoiEdge().p0, andEndPoint:edge.voronoiEdge().p1)
        line.lineWidth = 0.5;
        line.strokeColor = UIColor.blueColor()
        skScene.addChild(line)
        }
        }*/
        
        /*let map = Map()
        map.buildGraph(points, voronoi: delaunayVoronoi)
        
        for center in map.centers {
        let point = center.point
        var point2 = point;
        point2.x = point2.x+5
        let line = SKShapeNode()
        line.path = linePathWithStartPoint(point, andEndPoint:point2)
        line.lineWidth = 0.5;
        line.strokeColor = UIColor.redColor()
        skScene.addChild(line)
        
        for edge in center.borders {
        if edge.v0 != nil && edge.v1 != nil {
        let line = SKShapeNode()
        line.path = linePathWithStartPoint(edge.v0!.point, andEndPoint:edge.v1!.point)
        line.lineWidth = 0.5;
        line.strokeColor = UIColor.whiteColor()
        skScene.addChild(line)
        }
        }
        }*/
        
    }
    
    // generate random points for the verticies of the triangles
    func randomPointsWithLength(length: Int) -> NSArray {
        var points = NSMutableArray()
        
        for i in 0...length-1 {
            //let point = CGPointMake(CGFloat(arc4random_uniform(50+UInt32(self.frame.size.width-100))), CGFloat(arc4random_uniform(50+UInt32(self.frame.size.height-100))))
            //let point = CGPointMake(CGFloat(arc4random_uniform(UInt32(300))), CGFloat(arc4random_uniform(UInt32(300))))
            //let point = CGPointMake(CGFloat(arc4random_uniform(50+UInt32(self.size.width-100))), CGFloat(arc4random_uniform(50+UInt32(self.size.height-100))))
            
            let point = CGPointMake(rect.minX+CGFloat(arc4random_uniform(UInt32(rect.maxX-rect.minX))), rect.minY+CGFloat(arc4random_uniform(UInt32(rect.maxY-rect.minY))))
            
            let value = NSValue(CGPoint: point)
            points.addObject( value)
        }
        
        return points;
    }
    
    let NUM_LLOYD_RELAXATIONS = 2
    func randomPointsLloyd(numPoints:Int) -> NSArray {
        // We'd really like to generate "blue noise". Algorithms:
        // 1. Poisson dart throwing: check each new point against all
        //     existing points, and reject it if it's too close.
        // 2. Start with a hexagonal grid and randomly perturb points.
        // 3. Lloyd Relaxation: move each point to the centroid of the
        //     generated Voronoi polygon, then generate Voronoi again.
        // 4. Use force-based layout algorithms to push points away.
        // 5. More at http://www.cs.virginia.edu/~gfx/pubs/antimony/
        // Option 3 is implemented here. If it's run for too many iterations,
        // it will turn into a grid, but convergence is very slow, and we only
        // run it a few times.
        //var i:int, p:Point, q:Point, voronoi:Voronoi, region:Vector.<Point>;
        
        let rect = CGRectMake(0, 0, self.size.width, self.size.height);
        
        var points = randomPointsWithLength(numPoints)
        for i in 1...NUM_LLOYD_RELAXATIONS {
            var voronoi = DelaunayVoronoi(points: points as [AnyObject], plotBounds:rect)
            var newPoints = NSMutableArray()
            for region in voronoi.regions() {
                let nsArray = region as! NSArray
                println("r \(nsArray.count)")
                
                var center = CGPoint(x: 0, y: 0)
                
                if(nsArray.count > 0) {
                    
                    for value in nsArray {
                        var p = (value as! NSValue).CGPointValue()
                        println("p \(p.x) \(p.y)")
                        
                        center.x += p.x
                        center.y += p.y
                    }
                    
                    center.x /= CGFloat(nsArray.count)
                    center.y /= CGFloat(nsArray.count)
                }
                
                println("n \(center.x) \(center.y)")
                
                if( !(center.x == 0 && center.y == 0) ) {
                    newPoints.addObject(NSValue(CGPoint: center))
                }
            }
            //voronoi.dispose();
            points = newPoints
        }
        return points;
    }
    
    func savePoint(inout point:CGPoint, copyPoint:CGPoint) {
        point.x = copyPoint.x
        point.y = copyPoint.y
    }
    
    func linePathWithStartPoint(p0:CGPoint, andEndPoint p1:CGPoint) -> CGPathRef {
        var path = CGPathCreateMutable();
        CGPathMoveToPoint(path, nil, p0.x, p0.y);
        CGPathAddLineToPoint(path, nil, p1.x, p1.y);
        
        return path;
    }
}
