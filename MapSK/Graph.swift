//
//  Graph.swift
//  
//
//  Created by Juan Carlos Cubillos Ocampo on 8/1/15.
//
//

import Foundation

class Center: Equatable {
    var index:Int!
    
    var point:CGPoint!  // location
    var water:Bool!  // lake or ocean
    var ocean:Bool!  // ocean
    var coast:Bool!  // land polygon touching an ocean
    var border:Bool!  // at the edge of the map
    var biome:String!  // biome type (see article)
    var elevation:Float!  // 0.0-1.0
    var moisture:Float!  // 0.0-1.0
    
    var neighbors = [Center]()
    var borders = [Edge]()
    var corners = [Corner]()
}
func ==(lhs: Center, rhs: Center) -> Bool {
    return lhs.point == rhs.point
}

class Corner: Equatable {
    
    
    var index:Int!
    
    var point:CGPoint!  // location
    var water:Bool!  // lake or ocean
    var ocean:Bool!  // ocean
    var coast:Bool!  // land polygon touching an ocean
    var border:Bool!  // at the edge of the map
    var biome:String! // biome type (see article)
    var elevation:Float!  // 0.0-1.0
    var moisture:Float!  // 0.0-1.0
    
    var touches = [Center]()
    var protrudes = [Edge]()
    var adjacent = [Corner]()
    
    var river:Int!  // 0 if no river, or volume of water in river
    var downslope:Corner!  // pointer to adjacent corner most downhill
    var watershed:Corner!  // pointer to coastal corner, or null
    var watershed_size:Int!
    
    
}

func ==(lhs: Corner, rhs: Corner) -> Bool {
    return lhs.point == rhs.point
}

class Edge {
    var index:Int!
    var d0:Center!
    var d1:Center! // Delaunay edge
    var v0:Corner?
    var v1:Corner?  // Voronoi edge
    var midpoint:CGPoint?  // halfway between v0,v1
    var river:Int!  // volume of water, or 0
};