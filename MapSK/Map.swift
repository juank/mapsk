//
//  Map.swift
//  TestMapSK
//
//  Created by Juan Carlos Cubillos Ocampo on 8/2/15.
//  Copyright (c) 2015 Juan Carlos Cubillos Ocampo. All rights reserved.
//

import Foundation

extension CGPoint: Hashable {
    public var hashValue: Int {
        return self.x.hashValue << 32 ^ self.y.hashValue
    }
}

class Map {
    
    var centers=[Center]()
    var corners=[Corner]()
    var edges=[Edge]()
    let SIZE = CGFloat(300)
    
    func buildGraph(points:NSArray, voronoi:DelaunayVoronoi) {
        var libedges = voronoi.edges
        var centerLookup = [CGPoint: Center]()
        
        // Build Center objects for each of the points, and a lookup map
        // to find those Center objects again as we build the graph
        for value in points {
            var point = value.CGPointValue()
            var center = Center()
            center.index = centers.count
            center.point = point
            /*p.neighbors = [Center]()
            p.borders = [Edge]()
            p.corners = [Corner]()*/
            centers.append(center)
            centerLookup[point] = center;
        }
        
        // Workaround for Voronoi lib bug: we need to call region()
        // before Edges or neighboringSites are available
        /*for p in centers {
            voronoi.regionForPoint(p.point)
        }*/
        
        // The Voronoi library generates multiple Point objects for
        // corners, and we need to canonicalize to one Corner object.
        // To make lookup fast, we keep an array of Points, bucketed by
        // x value, and then we only have to look at other Points in
        // nearby buckets. When we fail to find one, we'll create a new
        // Corner object.
        var _cornerMap = [Int: [Corner]]()
        
        func makeCorner(point:CGPoint) -> Corner {
            var q:Corner
            
            //if point == nil { return nil }
            for var bucket = Int(point.x)-1; bucket <= Int(point.x)+1; bucket++ {
                if _cornerMap[bucket] != nil {
                    for q in _cornerMap[bucket]! {
                        var dx = point.x - q.point.x
                        var dy = point.y - q.point.y
                        if ( (dx*dx) + (dy*dy) < 1e-6) {
                            return q
                        }
                    }
                }
            }
            var bucket = Int(point.x);
            if (_cornerMap[bucket] == nil) { _cornerMap[bucket] = [] }
            q = Corner()
            q.index = corners.count;
            q.point = point;
            q.border = point.x == 0 || point.x == SIZE || point.y == 0 || point.y == SIZE
            /*q.touches = [Center]()
            q.protrudes = [Edge]()
            q.adjacent = [Corner]()*/
            _cornerMap[bucket]!.append(q);
            corners.append(q);
            return q;
        }
        
        // Helper functions for the following for loop; ideally these
        // would be inlined
        
        
        for libedge in libedges {
            var dedge = libedge.delaunayLine();
            var vedge = DelaunayLineSegment(leftCoordinate: libedge.leftClippedPoint, rightCoordinate: libedge.rightClippedPoint)
            
            // Fill the graph data. Make an Edge object corresponding to
            // the edge from the voronoi library.
            var edge = Edge();
            edge.index = edges.count;
            edge.river = 0;
            
            if vedge != nil {
                edge.midpoint = interpolate(vedge.p0, point2: vedge.p1, f: 0.5);
            } else {
                edge.midpoint = nil
            }
            // Edges point to corners. Edges point to centers.
            if vedge != nil {
                edge.v0 = makeCorner(vedge.p0);
                edge.v1 = makeCorner(vedge.p1);
            } else {
                edge.v0 = nil;
                edge.v1 = nil;
            }
            edge.d0 = centerLookup[dedge.p0];
            edge.d1 = centerLookup[dedge.p1];
            
            // Centers point to edges. Corners point to edges.
            if (edge.d0 != nil) { edge.d0!.borders.append(edge) }
            if (edge.d1 != nil) { edge.d1!.borders.append(edge) }
            if (edge.v0 != nil) { edge.v0!.protrudes.append(edge) }
            if (edge.v1 != nil) { edge.v1!.protrudes.append(edge) }
            
            // Centers point to centers.
            if (edge.d0 != nil && edge.d1 != nil) {
                addToCenterList(&edge.d0!.neighbors, x: edge.d1!)
                addToCenterList(&edge.d1!.neighbors, x: edge.d0!)
            }
            
            // Corners point to corners
            if (edge.v0 != nil && edge.v1 != nil) {
                addToCornerList(&edge.v0!.adjacent, x: edge.v1!);
                addToCornerList(&edge.v1!.adjacent, x: edge.v0!);
            }
            
            // Centers point to corners
            if (edge.d0 != nil) {
                if edge.v0 != nil {
                    addToCornerList(&edge.d0!.corners, x: edge.v0!)
                }
                if edge.v1 != nil {
                    addToCornerList(&edge.d0!.corners, x: edge.v1!)
                }
            }
            if (edge.d1 != nil) {
                if edge.v0 != nil {
                    addToCornerList(&edge.d1!.corners, x: edge.v0!)
                }
                if edge.v1 != nil {
                    addToCornerList(&edge.d1!.corners, x: edge.v1!);
                }
            }
            
            // Corners point to centers
            if (edge.v0 != nil) {
                addToCenterList(&edge.v0!.touches, x: edge.d0!);
                addToCenterList(&edge.v0!.touches, x: edge.d1!);
            }
            if (edge.v1 != nil) {
                addToCenterList(&edge.v1!.touches, x: edge.d0!);
                addToCenterList(&edge.v1!.touches, x: edge.d1!);
            }
            
            edges.append(edge);
        }
    }
    
    func interpolate(point1: CGPoint, point2: CGPoint, f: CGFloat) -> CGPoint {
        var interpolate = CGPoint(x: point1.x-point2.x, y: point1.y-point2.y)
        interpolate.x *= f
        interpolate.y *= f
        
        interpolate.x += point2.x
        interpolate.y += point2.y
        
        return interpolate
    }
    
     func addToCornerList(inout v:[Corner], x:Corner) {
        if (find(v, x) != nil) { v.append(x) }
    }
    
    func  addToCenterList(inout v:[Center], x:Center) {
        if (find(v, x) != nil) { v.append(x) }
    }
    
}