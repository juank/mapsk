This is my attempt to translate to Swift/IOS the procedural map generation project from:  
http://www-cs-students.stanford.edu/~amitp/game-programming/polygon-map-generation/  
https://github.com/amitp/mapgen2  
  
I used delaunay code from:  
https://github.com/czgarrett/delaunay-ios  
  
![](images/image1.jpg)

![](images/image2.jpg)

![](images/image3.jpg)